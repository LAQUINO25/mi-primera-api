const express = require('express');
const router = express.Router();
const productos = require('../models/productos');
console.log(productos)
const validar_indice = require('../middlewares/productos');
const sesion = require('../models/sesion');

router.get("/", function (req, res){

    res.json({"productos": productos})

})


router.post("/nuevo_producto", function (req, res) {
    if(sesion.usuario.admin == false){
        res.send('no es admin');
    }else {
       var nuevo_producto = {
        id: productos.length +1,
        nombre: req.body.nombre,
        precio: req.body.precio,
       }
    };

    productos.push(nuevo_producto);
    res.send("Producto creado");
    console.log(nuevo_producto);
});

router.put("/actualizar_producto", validar_indice, function (req, res){

    if(sesion.usuario.admin ==false){
        res.send('no es admin')
    }else{
        const productoFound = productos.findIndex(x => x.id == req.body.indice);

        productos[req.body.indice].nombre = req.body.nombre;
        productos[req.body.indice].precio = req.body.precio;
    
        res.send("Producto actualizado")
        
    }

    
})

router.delete("/", validar_indice, function (req, res){

    if(sesion.usuario.admin == false){
        res.send('no es admin')
    }else{
        productos.splice(req.body.indice,1)
  
        res.send("Producto eliminado")

    }
    
    
})


module.exports = router;
