const express = require('express');
const router = express.Router();
const pedidos = require('../models/pedidos');
const validar_indice = require('../middlewares/pedidos');
const usuarios = require('../models/usuarios');
const sesion = require('../models/sesion');
const mediosDePago = require('../models/mediosPago');
const pedidoCliente = require('../models/pedidos');
const { usuario } = require('../models/sesion');

//Listar todos los pedidos realizados por los clientes. Accion solo autorizada para el admin. 

router.get("/", function (req, res){
    if(sesion.usuario.admin == false){
        res.send('no es admin')
    }else{

        res.send({"pedidos": pedidos})

    }
    
})

// Que los usuarios comunes registrados puedan realizar pedidos de los productos y puedan agregar mas de un producto al pedido.
router.post("/nuevo_pedido", function (req, res){
    
        const detalle = req.body.detalle
        var nuevoPedido = {
            id: pedidos[pedidos.length- 1].id + 1, //autoincrementable
            id_usuario: sesion.usuario.id,
            direccion_destino: req.body.direccion_destino,
            metodo: req.body.metodo,
            estado: "pendiente",
            detalle: detalle 
        };
     
         pedidos.push(nuevoPedido);
         res.send("Pedido creado")
         console.log(nuevoPedido)
    
    
    
   
});

// Historial de pedido de cada cliente
router.get('/pedidoCliente', function (req, res){
   
    let pedidoEncontrado = pedidos.filter(x=>x.id_usuario == sesion.usuario.id);
    console.log(pedidoEncontrado);
    if (pedidoEncontrado == -1) {
       res.send('No se registran pedidos');
    }
  
      else{
       console.log(pedidoEncontrado);
       res.send(pedidoEncontrado)
      }
})




router.put("/actualizar_pedido", validar_indice, function (req, res){
    if(sesion.usuario.admin == false){
        res.send('no es admin');
    }else{
       
        pedidos[req.body.indice].estado = req.body.estado;

         res.send("Pedido actualizado")

    }
    
})

router.delete("/eliminar_pedido", validar_indice, function (req, res){

    if(sesion.usuario.admin == false){
        res.send('no es admin');
    }else{
        pedidos.splice(req.body.indice,1)
  
         res.send("Pedido eliminado")
    }
    
    
})


module.exports = router;
