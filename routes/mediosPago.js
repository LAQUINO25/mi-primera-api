const express = require('express');
const router = express.Router();
const mediosDePago = require('../models/mediosPago')
const validar_indice = require('../middlewares/mediosPago')
const sesion = require('../models/sesion');

router.get("/", function (req, res){

    if(sesion.usuario.admin == false){
        res.send('no es admin');
    }else {
        res.send({"mediosDePago": mediosDePago})

    }    

})


router.post("/nuevo_medio", function (req, res){
   if(sesion.usuario.admin == false){
       res.send('no es admin')
   }else{
    var nuevo_medio = {
        id: mediosDePago.length +1,
        metodo: req.body.metodo
    };
 
     mediosDePago.push(nuevo_medio)
     res.send("Medio de Pago creado")

   }

   
})


router.put("/actualizar_medio", function (req, res){

    if(sesion.usuario.admin == false){
        res.send('no es admin');
    }else{
        mediosDePago[req.body.indice].metodo = req.body.metodo;

        res.send("Metodo de pago actualizado")

    }
    
})

router.delete("/eliminar_medio", function (req, res){
    
    if(sesion.usuario.admin == false){
        res.send('no es admin');
    }else {
        mediosDePago.splice(req.body.indice,1)

        res.send("Metodo de pago eliminado")

    }
    
    
})

module.exports = router;