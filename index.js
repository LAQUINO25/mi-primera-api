const express = require('express');
const app = express ();
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const helmet = require('helmet');
const jwt = require('jsonwebtoken');




//llamado a la base de datos
require('./src/database/db');
require('./src/database/sync');

//Configuracion rutas 
//const apiRouter = require('./src/routes/api');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(helmet());


//Coniguracion inicial para las rutas 'api'
//app.use('/', apiRouter);

app.use('/usuarios', require('./src/routes/api/usuarios/usuarios'));
app.use('/productos', require('./src/routes/api/productos/productos'));
app.use('/mediosPago', require('./src/routes/api/mediosPago/mediosPago'));
app.use('/pedidos', require('./src/routes/api/pedidos/pedidos'));
app.use('/direcciones', require('./src/routes/api/direcciones/direcciones'));



//configuracion swaggger
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            tittle: 'Delilah Restó',
            version: '2.0.0'
        }
    },
    apis: ['./src/swagger/swagger.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);


app.use('/api-docs',
       swaggerUI.serve,
       swaggerUI.setup(swaggerDocs));

//

//app.use(require('./src/routes/api'));


//configuracion puerto
app.listen(3000, function(req, res){
    console.log('Servidor corriendo por el puerto 3000');
});
