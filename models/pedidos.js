const productos = require("./productos");

const pedidos = [
    {
        id: 1,
        id_usuario: 1,
        direccion_destno: "las violetas 123",
        metodo: "efectivo",
        estado: "pendiente",
        detalle: [{
            id_producto: 1,
            cantidad: 2,

        }]
    },
    {
        id: 2,
        id_usuario: 2,
        direccion_destino: "rivadavia 500",
        metodo: "tarjeta de credito",
        estado: "confirmado",
        detalle: [{
            id_producto: 1,
            cantidad: 2,

        },{
            id_producto: 2,
            cantidad: 1,
        }]
    }
]


module.exports = pedidos;

