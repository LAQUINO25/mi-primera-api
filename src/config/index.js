//configuracion variables de entorno
require('dotenv').config();

module.exports = {
    
    database: {
        username: process.env.MYSQL_USERNAME || 'root',
        password: process.env.MYSQL_PASSWORD || '',
        database: process.env.MYSQL_DATABASE || 'Delilah Resto',
        dialect: process.env.MYSQL_DIALECT || 'mysql',
        host: process.env.MYSQL_HOST || 'localhost',
        key: process.env.KEY || 'claveSecreta',
        secret: process.env.SECRET || 'ABC123',
        rounds: process.env.ROUNDS || 10
        
    
    }
}