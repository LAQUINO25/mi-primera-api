const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require('../database/db');
const detallePedido = require('../models/detallePedido');
//const  Usuario  = require('../models/usuarios');
const MedioDePago = require('../models/mediosPago');
const Direccion = require('../models/direcciones');

//MODELO DE TABLA PEDIDOS
class Pedido extends Model {}
Pedido.init({

    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    
    },
    direccionId: {
        type: DataTypes.INTEGER,
        reference: {
            model: 'Direcciones',
            key: 'id'
        }
    },
    pagoId: {
        type: DataTypes.INTEGER,
        reference: {
            model: 'Metodo de pago',
            key: 'id'
        }
    },
    estado: {
        type: DataTypes.STRING,
       
    }
},
{
    sequelize,
    modelName: 'Pedidos',
    timestamps: false
});

module.exports = Pedido;



Pedido.hasOne(MedioDePago);
Pedido.hasOne(detallePedido);