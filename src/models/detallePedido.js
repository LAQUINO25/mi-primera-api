const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize } = require('../database/db');
const Producto = require('../models/productos');

//MOELO DE TABLA DE LOS DETALLE DE LOS PEDIDOS
class detallePedido extends Model {}
detallePedido.init({

    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    cantidad: {
        type: DataTypes.DOUBLE,
        allowNull: false
    }
},
{
    sequelize,
    modelName: 'Detalle de pedido',
    timestamps: false
});

module.exports = detallePedido;

detallePedido.hasOne(Producto);
