const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require('../database/db');

//MODELO DE TABLA PARA LAS DIRECCIONES DE LOS US REGISTRADOS
class Direccion extends Model{}
Direccion.init({

    idDireccion: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    direccion: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    idUsuario: {
        type: DataTypes.INTEGER,
        allowNull: false
     }
    
},
{
    sequelize,
    modelName: 'Direcciones',
    timestamps: false
});

module.exports = Direccion;