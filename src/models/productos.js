const { Sequelize, DataTypes, Model} = require('sequelize');
const {sequelize} = require('../database/db');
//const detallePedido = require('../models/detallePedido');

//MODELO DE TABLA PRODUCTOS
class Producto extends Model {};
Producto.init ({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre:{
        type: DataTypes.STRING(100),
        allowNull: false
    },
    precio: {
        type: DataTypes.DOUBLE,
        allowNull: false
    }

  
},
{
    sequelize,
    modelName: 'Productos',
    timestamps: false
});

module.exports = Producto;

