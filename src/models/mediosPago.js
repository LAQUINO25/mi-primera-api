const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize } = require('../database/db');


//MODELO DE TABLA MEDIOS DE PAGO
class MedioDePago extends Model {}
MedioDePago.init ({

    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    metodo: {
        type: DataTypes.STRING,
        allowNull: false
    }
},
{
    sequelize,
    modelName: 'Metodo de pago',
    timestamps: false
});


module.exports = MedioDePago;