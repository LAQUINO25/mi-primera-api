const { Sequelize, DataTypes, Model } = require('sequelize');
const { sequelize } = require('../database/db');
const Pedido  = require('../models/pedidos');
const Direccion = require('../models/direcciones');

//MODELO DE TABLA BASE DE DATOS

class Usuario extends Model {}
Usuario.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Nombre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Apellido: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        Admin: {
            type: DataTypes.BOOLEAN,
        },
        Logueado: {
            type: DataTypes.BOOLEAN
        },
        Activo: {
            type: DataTypes.BOOLEAN
        }
    },
    {
        sequelize,
        modelName: 'Usuarios',
        timestamps: false
    }
);

module.exports = Usuario;

Usuario.hasMany(Direccion);
Usuario.hasMany(Pedido);
