
const router = require ('express').Router();
//const crear = require('../../../controllers/usuarios');
//const listar = require('../../../controllers/usuarios');
const Usuario = require('../../../models/usuarios');
const validar_email = require('../../../middlewares/usuarios');
const {estaLogueado} = require('../../../middlewares/usuarios');
const {esAdmin }= require('../../../middlewares/usuarios');
const { validarEstado } = require('../../../middlewares/usuarios');
const { autenticarUsuario } = require('../../../middlewares/usuarios');


    
const {
    crear,
    listar,
    login,
    suspender

} = require('../../../controllers/usuarios');

//SE CREA UN REGISTRO
router.post('/', crear);

// SE LISTAN TODOS LOS USUARIOS. SOLO EL ADMINISTRADOR PUEDE ACCEDER
router.get('/', estaLogueado, esAdmin, listar);

// SE LOGUEA UN USUARIO 
router.post('/login', validarEstado, login);

// SOLO EL ADMINISTRADOR PUEDE SUSPENDER A UN USUARIO
router.put('/:id', autenticarUsuario, esAdmin, suspender );




module.exports = router;