const router = require('express').Router();
const {estaLogueado} = require('../../../middlewares/usuarios');
const { esAdmin }= require('../../../middlewares/usuarios');


const {
   crear,
   listar,
   editar,
  eliminar
} = require('../../../controllers/pedidos')

//CREAR UN PEDIDO( SOLO USUARIOS REGISTRADOS)
router.post('/', estaLogueado, crear);

//LISTA D EPEDIDOS PARA TODOS LOS USUARIOS REGISTRADOS
router.get('/', estaLogueado, listar);

//EDITAR PEDIDO(SOLO ADMIN)
router.put('/:id', estaLogueado, esAdmin, editar);

//ELIMAR UN PEDIDO(SOLO ADMIN)
router.delete('/:id', estaLogueado, esAdmin, eliminar);


module.exports = router;