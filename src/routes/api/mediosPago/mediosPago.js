const router = require('express').Router();
const { esAdmin} = require('../../../middlewares/usuarios');
const {estaLogueado} = require('../../../middlewares/usuarios');

const{
    crear,
    listar,
    editar,
    eliminar
} = require('../../../controllers/mediosPago')

//CREAR MEDIOS DE PAGO (SOLO ADMIN)
router.post('/', estaLogueado, esAdmin, crear);

// SE LISTAN TODOS LOS MEDIOS DE PAGO SOLO PARA USUARIOS REGISTRADOS
router.get('/', estaLogueado,listar);

//EDITAR UN MEDIO DE PAGO(SOLO ADMIN)
router.put('/:id', estaLogueado, esAdmin, editar);

//ELIMINAR UN MEDIO DE PAGO(SOLO ADMIN)
router.delete('/:id', estaLogueado, esAdmin, eliminar);


module.exports = router;
