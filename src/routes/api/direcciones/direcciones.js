const router = require('express').Router();

const { estaLogueado } = require('../../../middlewares/usuarios');
const { listaDireccion, crearDireccion} = require('../../../controllers/direcciones');

//AGENDA DE DIRECCIONES SOLO PARA USUARIOS REGISTRADOS
router.get('/', estaLogueado, listaDireccion);

// CREAR DIRECCIONES PARA UN USUARIO REGISTRADO
router.post('/', estaLogueado, crearDireccion);

module.exports = router;

