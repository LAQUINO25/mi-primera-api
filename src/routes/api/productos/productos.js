const router = require('express').Router();
const {estaLogueado, validarEstado} = require('../../../middlewares/usuarios');
const { esAdmin } = require('../../../middlewares/usuarios');
const { validar_indice } = require('../../../middlewares/productos');


const{
    crear,
    listar,
    editar,
    eliminar
} = require('../../../controllers/productos')


//CREAR UN PRODUCTO (SOLO ADMINISTRADOR)
router.post('/', estaLogueado, esAdmin, crear);

//LISTA DE PRODUCTOS DISPONIBLE PARA TODOS LOS USUARIOS REGISTRADOS
router.get('/',estaLogueado, validarEstado, listar);

//EDITAR PRODUCTO(SOLO ADMIN)
router.put('/:id', esAdmin, validar_indice, editar);

//ELIMINAR PRODUCTO(SOLO ADMIN)
router.delete('/:id', esAdmin, validar_indice, eliminar);


module.exports = router;
