
//documentacion usuarios

/**
 * @swagger
 * /usuarios:
 *  get:
 *    tags:
 *    - usuarios
 *    description: Listado de usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /usuarios:
 *  post:
 *    tags:
 *    - usuarios
 *    description: Crear nuevo usuario
 *    parameters:
 *    - name: Nombre
 *      type: string
 *      in: formData
 *      required: false
 *      description: Nombre del usuario
 *    - name: Apellido
 *      type: string
 *      in: formData
 *      required: false
 *      description : Apellido del usuario
 *    - name: Email
 *      type: string
 *      in: formData
 *      required: false
 *      description : Email del usuario
 *    - name: Password
 *      type: string
 *      in: formData
 *      required: false
 *      description : Password del usuario
 *    - name: Admin
 *      type: string
 *      in: formData
 *      required: false
 *      description : Rol del usuario
 *    - name: Logueado
 *      type: string
 *      in: formData
 *      required: false
 *      description : Estado del usuario
 *    - name: Activo
 *      type: string
 *      in: formData
 *      required: false
 *      description : Estado del usuario
 *    responses:
 *      200:
 *        Success
 */




/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    tags:
 *    - usuarios
 *    description: Login de usuario
 *    parameters:
 *    - name: Email
 *      type: string
 *      in: formData
 *      required: false
 *      description: Email del usuario
 *    - name: Password
 *      type: string
 *      in: formData
 *      required: false
 *      description : Password del usuario
 *    responses:
 *      200:
 *        Success
 */


/**
 * @swagger
 * /usuarios/:id:
 *  put:
 *    tags:
 *    - usuarios
 *    description: Suspender un usuario
 *    parameters:
 *    - name: Email
 *      type: string
 *      in: formData
 *      required: false
 *      description: Email del usuario suspendido
 *    responses:
 *      200:
 *        Success
 */





//DOCUMENTACIN PRODUCTOS

/**
 * @swagger
 *   /productos:
 *   get:
 *     tags:
 *     - productos
 *     description: Lista de productos
 *     responses: 
 *       200:
 *         Success
 *     
 */


/**
 * @swagger
 * /productos:
 *  post:
 *    tags:
 *    - productos
 *    description: Crear producto
 *    parameters:
 *    - name: nombre
 *      type: string
 *      in: formData
 *      required: false
 *      description: Nombre del producto
 *    - name: precio
 *      type: interger
 *      in: formData
 *      required: false
 *      description : Precio del producto
 *    responses:
 *      200:
 *        Success
 */


/**
 * @swagger
 * /productos:
 *  put:
 *    tags:
 *    - productos
 *    description: Actualizar producto
 *    parameters:
 *    - name: nombre
 *      type: string
 *      in: formData
 *      required: false
 *      description: Nombre del producto
 *    - name: precio
 *      type: interger
 *      in: formData
 *      required: false
 *      description : Precio del producto
 *    responses:
 *      200:
 *        Success
 */



/**
 * @swagger
 * /productos:
 *  delete:
 *    tags:
 *    - productos
 *    description: Eliminar producto
 *    parameters:
 *    - name: nombre
 *      type: string
 *      in: formData
 *      required: false
 *      description: Nombre del producto
 *    - name: precio
 *      type: interger
 *      in: formData
 *      required: false
 *      description : Precio del producto
 *    responses:
 *      200:
 *        Success
 */


// documentacion pedidos

/**
 * @swagger
 *   /pedidos:
 *   get:
 *     tags:
 *     - pedidos
 *     responses: 
 *       200:
 *         Success
 *     
 */


/**
 * @swagger
<<<<<<< HEAD:swagger/swagger.js
 *   /pedidos/pedidoCliente:
 *   get:
 *     tags:
 *     - pedidos
 *     responses: 
 *       200:
 *         Success
 *     
 */

/**
 * @swagger
 * /pedidos/nuevo_pedido:
=======
 * /pedidos:
>>>>>>> persistencia:src/swagger/swagger.js
 *  post:
 *    tags:
 *    - pedidos
 *    description:  Crear pedido
 *    parameters:
 *    - name: usuarioId
 *      type: integer
 *      in: formData
 *      required: false
 *      description: Id de usuario
 *    - name: direccionEntrega
 *      type: string
 *      in: formData
 *      required: false
 *      description : Direccion de entrega
 *    - name: estado
 *      type: string
 *      in: formData
 *      required: false
 *      description : Estad del pedido
 *    - name: cantidad
 *      type: integer
 *      in: formData
 *      required: false
 *      description : Cantidad de productos
 *    - name: idProducto
 *      type: integer
 *      in: formData
 *      required: false
<<<<<<< HEAD:swagger/swagger.js
 *      description : cantidad de productos
 *    - name: direccion
 *      type: string
 *      in: formData
 *      required: false
 *      description : direccion de destino
 *    - name: metodo
 *      type: string
 *      in: formData
 *      required: false
 *      description : metodo de pago
=======
 *      description : Id del producto seleccionado
>>>>>>> persistencia:src/swagger/swagger.js
 *    responses:
 *      200:
 *        Success
 */

/**
 * @swagger
 * /pedidos/:id:
 *  put:
 *    tags:
 *    - pedidos
 *    description:  Actualizar pedido
 *    parameters:
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: false
 *      description: Id del pedido
 *    - name: estado
 *      type: string
 *      in: formData
 *      required: false
 *      description : estado del pedido
 *    responses:
 *      200:
 *        Success
 */


/**
 * @swagger
 * /pedidos/:id:
 *  delete:
 *    tags:
 *    - pedidos
 *    description:  eliminar pedido
 *    parameters:
 *    - name: id
 *      type: interger
 *      in: formData
 *      required: false
 *      description: indice del pedido
 *    responses:
 *      200:
 *        Success
 */


// documentacion medios de pago

/**
 * @swagger
 *   /medios:
 *   get:
 *     tags:
 *     - medios de pago
 *     responses: 
 *       200:
 *         Success
 *     
 */

/**
 * @swagger
 * /mediosPago:
 *  post:
 *    tags:
 *    - medios de pago
 *    description: Crear medio de pago
 *    parameters:
 *    - name: metodo
 *      type: string
 *      in: formData
 *      required: false
 *      description: Nombre del medio
 *    responses:
 *      200:
 *        Success
 */

/**
 * @swagger
 * /mediosPago/:id:
 *  put:
 *    tags:
 *    - medios de pago
 *    description: Actualizacion de medio de pago
 *    parameters:
 *    - name: id
 *      type: interger
 *      in: formData
 *      required: false
 *      description: id del medio
 *    - name: metodo
 *      type: string
 *      in: formData
 *      required: false
 *      description: nombre del metodo
 *    responses:
 *      200:
 *        Success
 */

/**
 * @swagger
 * /mediosPago/:id:
 *  delete:
 *    tags:
 *    - medios de pago
 *    description: Eliminar medio de pago
 *    parameters:
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: false
 *      description: id del medio
 *    responses:
 *      200:
 *        Success
 */

