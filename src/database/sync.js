//sincronizacion de las tablas con la base de datos

const { Sequelize } = require('sequelize');
const { sequelize } = require('./db');

const { Usuarios } = require('../models/usuarios');
const { Productos } = require('../models/productos');
const { Pedido } = require('../models/pedidos');
const { MedioDePago } = require('../models/mediosPago');
const { Direccion } = require('../models/direcciones');
const { detallePedido } = require('../models/detallePedido');

//SINCRONIZACION TABLAS
sequelize.sync({force:true}).then(() =>{
    console.log('Tablas sincronizadas');
});


//SE EXPORTAN LAS TABLAS
module.exports = {
    Usuarios,
    Productos,
    Pedido,
    MedioDePago,
    Direccion,
    detallePedido
};
