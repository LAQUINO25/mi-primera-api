
const { Sequelize } = require('sequelize');

const { database } = require('../config/index');

// Crear conexion con base de datos
const sequelize = new Sequelize ('Delilah Resto', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'

})

validar_conexion()

//VALIDA QUE LA CONEXION SE REALICE CORRECTAMENTE
async function validar_conexion(){
    try {
        await sequelize.authenticate();
        console.log('La conexion se extablecio correctamente');
    } catch (error){
        console.log('Error al conectarse a la base de datos:', error);
    }
}


module.exports = {
    sequelize
}