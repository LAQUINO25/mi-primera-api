const Direccion = require('../models/direcciones');

//Listar direcciones 

const listaDireccion = async (req, res)=>{
    const direcciones = await Direccion.findAll({
        where: {idUsuario: req.body.idUsuario}
    })
    res.json(direcciones)
}


//CREAR DIRECCION 
const crearDireccion = async (req,res)=>{
    const {direccion, idUsuario} = req.body
    
    if(!direccion || !idUsuario){
        res.json('debe ingresar todos los campos')
    }else {

        const datosDir = {
            idUsuario : req.body.idUsuario,
            direccion:req.body.direccion

        }

        const nuevaDireccion = await Direccion.create(datosDir);
        res.json({
            mensaje: 'nueva direccion agregada',
            dir_data: nuevaDireccion
        })
    }
}

module.exports = {
    crearDireccion,
    listaDireccion
}