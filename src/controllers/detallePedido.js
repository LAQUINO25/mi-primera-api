const detallePedido = require('../models/detallePedido');

//CREAR UN DETALLE DE PEDIDO PARA QUE EL US PUEDA AGREGAR MAS DE UN PRODUCTO A SU PEDIDO 
const crearDetalle = async (req, res) =>{
    console.log(req.body)

    const detalle = {
        productoId: req.body.productoId,
        cantidad: req.body.cantidad
        
    }
    const detPedido = await detallePedido.create(detalle);
    return res.json(detPedido)
}


module.exports = crearDetalle;