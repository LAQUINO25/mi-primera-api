
const Producto = require('../models/productos');


//CREAR PRODUCTO
const crear = async (req, res) =>{
    console.log(req.body)

    const prod = {
        nombre: req.body.nombre,
        precio: req.body.precio
    }

    const producto = await Producto.create(prod);
    return res.json(producto)
}

//LISTA DE PRODUCTOS
const listar = async (req, res) =>{
    console.log(req.body)

    const listaProd = await Producto.findAll();
    res.json({
        mensaje: 'Ok',
        respuesta: listaProd
    });
};


//EDITAR PRODUCTOS
const editar = async (req, res) =>{
    console.log(req.body)

    const editarProd = await Producto.update(
        {precio: req.body.precio},
        {where: {
            id: req.body.id
        }
    })
        
    return res.json(editarProd)
}


//ELIMINAR PRODUCTOS
const eliminar = async (req, res) => {
    console.log(req.body)

    const eliminarProd = await Producto.destroy({
        where: 
            {id: req.body.id}
        
    })
    res.send('Producto eliminado')
}


module.exports = {
    listar,
    crear,
    editar,
    eliminar
}