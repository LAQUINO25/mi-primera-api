const MedioDePago = require('../models/mediosPago');


//CRER MEDIO DE PAGO
const crear = async (req, res) =>{
    console.log(req.body)

    const medioPago = {
        metodo: req.body.metodo
    }

    const pago = await MedioDePago.create(medioPago);
    return res.json(pago)
}


//LISTAR MEDIO DE PAGO
const listar = async (req, res) =>{
    console.log(req.body)

    const listaPago = await MedioDePago.findAll();
    res.json({
        mensaje: 'Ok',
        respuesta: listaPago
    });
};



//EDITAR MEDIO DE PAGO
const editar = async (req, res) =>{
    console.log(req.body)

    const editarPago = await MedioDePago.update(
        {metodo: req.body.metodo},
        {where:{id: req.body.id} }
    )
        
    res.json({
        mensaje: 'Medio de pago actualizado',
        pago_edit: editarPago 
    })
}


//ELIMINAR MEDIO DE PAGO
const eliminar = async (req, res) => {
    console.log(req.body)

    const eliminarPago = await MedioDePago.destroy(
        {where: {id: req.body.id}
        
    })
    res.send('Medio de pago eliminado')
}


module.exports = {
    listar,
    crear,
    editar,
    eliminar
}