
const Usuario = require('../models/usuarios');
const {key, secret, rounds} = require('../config/index').database;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');



//CREAR USUARIO 
const crear = async (req, res) =>{

    const {Nombre, Apellido, Email, Password} = req.body;


    // SE CREA EL US EN LA BASE DE DATOS  Y SE ENCRIPTA CONTRASEÑA

    const usuarios = await Usuario.findOrCreate({
        where:  {Email: req.body.Email},
        defaults: {
            Nombre: req.body.Nombre,
            Apellido: req.body.Apellido,
            Email: req.body.Email,
            Password:bcrypt.hashSync(req.body.Password, Number.parseInt(rounds)), //SE ENCRIPTA CONTRASEÑA
            Admin: req.body.Admin,
            Logueado: req.body.Logueado,
            Activo: req.body.Activo
           
        }
        
    })
    return res.json(usuarios);
}

//LISTADO DE USUARIOS
const listar = async (req, res) =>{
    console.log(req.body);


    const listaUsu = await Usuario.findAll();
           res.json({
               mensaje: 'Ok',
               respuesta: listaUsu
           })

    
};


//LOGIN DE USUARIO Y TOKEN
const login = async (req, res) => {

    const datos = req.body;

    const loginUsuario = await Usuario.findOne(
        {where: {Email: req.body.Email, Password: req.body.Password}}
    )
    if(!loginUsuario){
        res.json({
            mensaje: "Registro incorrecto",
            LoginData: loginUsuario
        })
    }else {
       
        //SE CREA EL TOKEN
        const token = jwt.sign({
            datos: datos.Email,
            rol: ['admin'],
            
        }, key)
         
        // SI ENCUENTRA EL USUARRIO CAMBIA EL ESTADO DE LOGUEADO A TRUE
        const modificaUsuario = await Usuario.update(
            {Logueado: true},
            {where: {Email:loginUsuario.Email}}
        )
        res.json({
            mensaje: "Usuario logueado correctamente",
            data_log: [modificaUsuario, token]
    
        })
    }
}

//SUSPENDER UN USUARIO (SOLO ADMIN)

const suspender = async (req, res) =>{
    const id = req.body.id;

    const suspender = await Usuario.update({ Activo: false },{
        where: {
            Email: req.body.Email
        }
    })
    res.json({
        mensaje: 'Usuario suspendido',
        activo: suspender
    })
};









module.exports = {
    listar,
    crear,
    login,
    suspender,

}
    
    