
const Pedido = require('../models/pedidos');


//CREAR UN PEDIDO
const crear = async (req, res) =>{
    console.log(req.body)


   const pedido = {
        usuarioId: req.body.usuarioId,
        direccionEntrega: req.body.direccionEntrega,
        estado: req.body.estado,
        
    }

    const datoDetalle = {
        cantidad: req.body.cantidad,
        idProducto: req.body.idProducto
    }
        
    

    const pedidos = await Pedido.create(pedido);
    const detalle = await Pedido.create(datoDetalle);
    return res.json({
        mensaje: "Pedido creado con exito",
        respuesta: [pedidos, detalle]
    });
};


//LISTA DE PEDIDOS
const listar = async (req, res) =>{
    console.log(req.body)

    const listaPedidos = await Pedido.findAll();
    res.json({
        mensaje: 'Ok',
        respuesta: listaPedidos
    });
};

//EDITAR UN PEDIDO
const editar = async (req, res) =>{
    console.log(req.body)

    const editarPedido = await Pedido.update(
        {estado: req.body.estado},
        {where: {
            id: req.body.id
        }
    })
        
    return res.json({
        mensaje: 'Pedido actualizado',
        edit_pedido: editarPedido
    })
}


//ELIMINAR PEDIDOS
const eliminar = async (req, res) => {
    console.log(req.body)

    const eliminarPedido = await Pedido.destroy({
        where: 
            {id: req.body.id}
        
    })
    res.json({
        mensaje: 'Pedido eliminado',
        eliminar: eliminarPedido
    })
}


module.exports = {
    listar,
    crear,
    editar,
    eliminar
}