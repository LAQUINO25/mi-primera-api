//const usuarios = require('../models/usuarios');
const Usuario = require('../models/usuarios')
const {Op} = require('sequelize');
const jwt= require('jsonwebtoken');
const { key} = require('../config/index').database;




//Validar campos vacios 
const valCampos = async (req, res, next)=>{
    const {Email, Password} = req.body;
    if (Email === '' && Password === '') {
        res.send('Verificar que los campos no esten vacios');
    }else 
    next();
    
}


//Validar que este el usuario esté logueado
const estaLogueado = async  (req, res, next) => {



    const usuarioLogueado = await Usuario.findOne(
    
            {where:{
               Logueado: true
                
            }
        })

        if(!usuarioLogueado){
            res.json('Debe registrarse')
        }else {
            next();
        };
       
        

};

//VALIDAR QUE SEA ADMIN
const esAdmin = async (req, res, next) => {


    const admin =await Usuario.findOne( 
    
        {where:{ Admin: true}}
        )
    if(!admin){
        res.send('No es admiistrador, no tiene permiso')
    }else {
        next();
    }
}

//Validar que la cuenta no esté suspendida
const validarEstado = async(req, res, next)=>{
    const {Email} = req.body;
   const usuar = await Usuario.findOne({
            where:{Activo: true}
   })
     

   if(!usuar){
       console.log('Cuenta suspendida');
   }else {
       next();
   }
}


//valiacion de usuario con jwt
const autenticarUsuario = async(req, res, next)=>{
    try{
        const token = req.headers.authorization.replace('Bearer', '')
        jwt.verify(token, key);
        next();
    }catch(error){
        res.status(400).send('Error al validar el usuario')
    }

};



module.exports = {
    estaLogueado,
    esAdmin,
    validarEstado,
    valCampos,
    autenticarUsuario

}
