const Producto = require('../models/productos');


//VALIDA EL INDICE DE LOS PRODUCTOS
const validar_indice = async (req, res, next) => {
    const productId = await Producto.findOne(
        {where: {id: req.body.id}}
    )
    if(productId >=0 && productId < Producto.length){
        next();
    } else {
      res.json({"mensaje": "invalido"})
    }

}

module.exports = {
    validar_indice
};
