const chai = require('chai');
const expect = require('chai').expect;
//const fetch = require('node-fetch');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const url = 'http://localhost:3000'

describe('Evaluacion para el registro de usuarios',function (){
    it('Usuario registrado correctamente', (done) =>{
        chai.request(url).post('/api/usuarios/crear').send({
            Nombre: "Laura",
            Apellido: "Aquino",
            Email: "lala@admin.com",
            Password: "2590",
            Admin: true,
            Logueado: false,
            Activo: true
        }).end(function (err,res){
            console.log(res.body)
            expect(res).to.have.status(202);
            done();
        });
    });
});